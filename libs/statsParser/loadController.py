import sys
import os
import re
import getpass
import datetime
import pytz
import time
import itertools
import django.utils.timezone
from libs.statsParser.models import Player
from libs.statsParser.models import Owner
from libs.statsParser.models import LastSaved
from libs.statsParser import WebClient
from libs.statsParser import PlayerParser
from libs.statsParser import ValueBasedCalculator
from libs.statsParser import LeagueSettings

from bs4 import BeautifulSoup

class LoadController:
  def __init__(self):
    self.webClient = WebClient()
    self.playerParser = PlayerParser()
    self.valueBasedCalculator = ValueBasedCalculator(LeagueSettings())

  def parseAll(self, username, password):
    offset = 0
    while offset < 500:
      players = self.parseOffset(username, password, offset)
      offset = offset + len(players)
 
    self.parseDefense(username, password)

    LastSaved.objects.all().delete()
    lastSaved = LastSaved(lastSaved = django.utils.timezone.now())
    lastSaved.save()


  def parseOffset(self, username, password, offset):
    if offset == 0:
      Player.objects.all().delete()

    self.webClient.login(username, password)
    
    print 'Getting HTML ' + time.ctime()

    statsHtml = self.webClient.getPlayerStats(offset)
    players = self.playerParser.parse(statsHtml)

    print 'Starting save ' + time.ctime()
    
    for player in players:
      player.save()
    
    print 'Finished save ' + time.ctime()

    return players

  def parseDefense(self, username, password):
    self.webClient.login(username, password)

    statsHtml = self.webClient.getDefenseStats()
    players = self.playerParser.parse(statsHtml)
    
    statsHtml = self.webClient.getKickerStats()  
    players.extend(self.playerParser.parse(statsHtml))

    for player in players:
      player.save()
    
    return players

  def getAll(self):

    lastSaved = LastSaved.objects.all()[0].lastSaved
    central = pytz.timezone('America/Chicago')
    lastSavedDisplay = lastSaved.astimezone(central).strftime("%Y-%m-%d %l:%M%p %Z")

    players = Player.objects.all()
    owners = []
    
    self.valueBasedCalculator.calculateAveragePoints(players)
    self.valueBasedCalculator.calculateValueBasedPoints(players)

    players = sorted(players, key=Player.sortByOwner)
    for name, ownedPlayers in itertools.groupby(players, Player.sortByOwner):
      owners.append(Owner(name, sum([ownedPlayer.auctionValue for ownedPlayer in ownedPlayers if ownedPlayer.auctionValue > -50])))

    owners = sorted(owners, key=Owner.sortByTotalAuctionValue, reverse=True)
   

    #return (owners, [player for player in players if player.auctionValue > 0 or not player.owner == 'Waivers'])
    return (owners, players, lastSavedDisplay)

  
  def loadAuctionValues(self, username, password):
    self.webClient.login(username, password)
    offset = 0
    players = []    
    while offset <= 200:
      statsHtml = self.webClient.getAuctionValueStats(offset)
      players.extend(self.playerParser.parseAuctionValueStats(statsHtml))
      offset = len(players)

    existingPlayers = Player.objects.all()

    for existingPlayer in existingPlayers:
      for player in players:
        if player.name == existingPlayer.name and player.team == existingPlayer.team:
          existingPlayer.projectedAuctionValue = player.projectedAuctionValue
          existingPlayer.save()

    return self.getAll()










