import datetime

class LeagueSettings:
  def __init__(self):
    self.players = 12
    self.auctionBudget = 200
    self.starters = {}
    self.starters['QB'] = 1
    self.starters['RB'] = 4
    self.starters['WR'] = 4
    self.starters['TE'] = 1
    self.starters['K'] = 1
    self.starters['DEF'] = 1

    self.totalWeeks = 17
    self.firstTuesday = datetime.date(2014, 9, 9)
    self.byesByWeek = { 4 : ['Ari', 'Cin', 'Cle', 'Den', 'Stl', 'Sea'],
                        5 : ['Mia', 'Oak'],
                        6 : ['KC', 'NO'],
                        7 : ['Phi', 'TB'],
                        8 : ['NYG', 'SF'],
                        9 : ['Atl', 'Buf', 'Chi', 'Det', 'GB', 'Ten'],
                       10 : ['Hou', 'Ind', 'Min', 'NE', 'SD', 'Was'],
                       11 : ['Bal', 'Dal', 'Jac', 'NYJ'],
                       12 : ['Car', 'Pit']}


  def getWorstStarterPosition(self, position):
    return int(self.players * self.starters[position])

  def getWeeksRemaining(self, today):
    return self.totalWeeks - self.__getCurrentWeek(today) + 1

  def getTeamsWithByes(self, today):
    result = []
    for key in self.byesByWeek.keys():
      if key >= self.__getCurrentWeek(today):
        result.extend(self.byesByWeek[key])
    return result

  def __getCurrentWeek(self, today):
    for i in range(self.totalWeeks):
      if today < self.firstTuesday + datetime.timedelta(i * 7):
        return i + 1
    return self.totalWeeks
  

