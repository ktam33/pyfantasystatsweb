import datetime
from libs.statsParser.models import Player

class ValueBasedCalculator:

  baselineByPosition = {}

  def __init__(self, leagueSettings):
    self.leagueSettings = leagueSettings

  def calculateAveragePoints(self, players):
    weeksRemaining = self.leagueSettings.getWeeksRemaining(datetime.date.today())
    byeWeekTeams = self.leagueSettings.getTeamsWithByes(datetime.date.today())

    for player in players:
      if (player.team in byeWeekTeams):
        weeksRemainingForPlayer = weeksRemaining - 1
      else:
        weeksRemainingForPlayer = weeksRemaining
      
      player.averagePoints = float(player.points) / weeksRemainingForPlayer

  
  def calculateValueBasedPoints(self, players):
    totalBudget = self.leagueSettings.auctionBudget * self.leagueSettings.players
    totalVbdPoints = 0
    for player in players:
      player.vbdValue = player.averagePoints - self.__getBaselinePlayer(player.position, players).averagePoints
      if player.vbdValue > 0:
        totalVbdPoints += player.vbdValue

    valuePerPoint = totalBudget / totalVbdPoints

    for player in players:
      player.auctionValue = valuePerPoint * player.vbdValue
      player.auctionValueDiff = player.auctionValue - player.projectedAuctionValue


  def __getBaselinePlayer(self, position, players):
    if not position in self.baselineByPosition:
      worstStarterPosition = self.leagueSettings.getWorstStarterPosition(position)

      positionPlayers = [player for player in players if player.position == position]
      positionPlayers = sorted(positionPlayers, key=Player.sortByAveragePointsKey, reverse=True)
      self.baselineByPosition[position] = positionPlayers[worstStarterPosition - 1]
    return self.baselineByPosition[position]

  