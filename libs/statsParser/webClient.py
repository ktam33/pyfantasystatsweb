import urllib
import urllib2
import cookielib

class WebClient:
  def __init__(self):
    cj = cookielib.CookieJar()
    self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

  def login(self, username, password):
    data = urllib.urlencode([('login', username),('passwd', password)])
    self.opener.open("https://login.yahoo.com/config/login?", data)

  def getPlayerStats(self, offset):
    url = "http://football.fantasysports.yahoo.com/f1/537438/players?status=ALL&pos=O&cut_type=9&stat1=S_PSR_2014&myteam=0&sort=PTS&sdir=1&count=%d" % offset
    print url
    stats = self.opener.open(url)
    return stats.read()

  def getDefenseStats(self):
    url = "http://football.fantasysports.yahoo.com/f1/537438/players?status=ALL&pos=DEF&cut_type=9&stat1=S_PSR_2014&myteam=0&sort=PTS&sdir=1&count=0"
    print url
    stats = self.opener.open(url)
    return stats.read()    

  def getKickerStats(self):
    url = "http://football.fantasysports.yahoo.com/f1/537438/players?status=ALL&pos=K&cut_type=9&stat1=S_PSR_2014&myteam=0&sort=PR&sdir=1&count=0"
    print url
    stats = self.opener.open(url)
    return stats.read()    

  def getAuctionValueStats(self, offset):
    url = "http://football.fantasysports.yahoo.com/f1/537438/draftanalysis?tab=AD&pos=ALL&sort=DA_PC&count=%d" % offset
    print url
    stats = self.opener.open(url)
    return stats.read()
