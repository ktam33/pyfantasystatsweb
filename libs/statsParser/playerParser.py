from libs.statsParser.models import Player
from bs4 import BeautifulSoup

class PlayerParser:
  def parse(self, html):
    players = []
    soup = BeautifulSoup(html)
    playersTable = soup.find(id='players-table').find('tbody')
    rows = playersTable("tr")

    for row in rows:
      players.append(self.__parsePlayerRow(row))

    return players

  def __parsePlayerRow(self, row):
    cells = row.find_all("td")
    name = cells[1].find("a", "name").get_text()
    teamAndPos = cells[1].find("span", "Fz-xxs").get_text().split(' - ')
    team = teamAndPos[0]
    position = teamAndPos[1]

    if ',' in position: position = position.split(',')[0]

    owner = cells[3].get_text()
    if owner.startswith('W ('):
        owner = 'Waivers'
    points = float(cells[4].get_text())

    return Player(name=name, team=team, owner=owner, position=position, points=points)

  def parseAuctionValueStats(self, html):
    players = []
    soup = BeautifulSoup(html)
    playersTable = soup.find(id='draftanalysistable').find('tbody')
    rows = playersTable("tr")

    for row in rows:
      players.append(self.__parseAuctionValueRow(row))

    return players    


  def __parseAuctionValueRow(self, row):
    cells = row.find_all("td")
    name = cells[1].find("a", class_="name").get_text()
    teamAndPos = cells[1].find("span", class_="Fz-xxs").get_text().split(' - ')
    team = teamAndPos[0]
    position = teamAndPos[1]

    if ',' in position: position = position.split(',')[0]

    projectedAuctionValue = float(cells[-3].get_text()[1:])

    return Player(name=name, team=team, position=position, projectedAuctionValue=projectedAuctionValue)