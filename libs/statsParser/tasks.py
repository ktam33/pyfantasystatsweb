from celery import task
from libs.statsParser import loadController

@task
def add(x, y):
    return x + y

@task
def loadStats():
    controller = loadController.LoadController()
    controller.parseAll('ktam33', '')
    controller.loadAuctionValues('ktam33', '')

@task
def loadAuction():
    controller = loadController.LoadController()
    controller.loadAuctionValues('ktam33', '')