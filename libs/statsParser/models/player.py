from django.db import models

class Player(models.Model):
  name = models.CharField(max_length=200)
  team = models.CharField(max_length=10)
  owner = models.CharField(max_length=200)
  position = models.CharField(max_length=10)
  points = models.FloatField()
  averagePoints = models.FloatField(null=True)
  vbdValue = models.FloatField(null=True)
  auctionValue = models.FloatField(null=True)
  projectedAuctionValue = models.FloatField(default = 0)
  auctionValueDiff = models.FloatField(null=True)

  def __str__(self):
    return (self.name + ', ' + 
           self.team + ', ' + 
           self.position + ', ' + 
           self.owner + ', ' + 
           str(self.points) + ', ' + 
           str(self.averagePoints) + ', ' + 
           str(self.vbdValue) + ', ' + 
           str(self.averagePoints))

  def sortByPointsKey(self):
    return self.points

  def sortByAveragePointsKey(self):
    return self.averagePoints

  def sortByValueKey(self):
    return self.vbdValue

  def sortByDiff(self):
    return self.auctionValueDiff

  def sortByOwner(self):
    return self.owner

  class Meta:
    app_label = 'statsParser'