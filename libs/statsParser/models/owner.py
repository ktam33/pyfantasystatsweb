class Owner:
  name = ""
  totalAuctionValue = 0

  def __init__(self, name, totalAuctionValue):
  	self.name = name
  	self.totalAuctionValue = totalAuctionValue

  def sortByTotalAuctionValue(self):
    return self.totalAuctionValue