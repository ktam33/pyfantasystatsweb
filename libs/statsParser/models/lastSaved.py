from django.db import models

class LastSaved(models.Model):
    lastSaved = models.DateTimeField()
    class Meta:
        app_label = 'statsParser'
