from django.test import TestCase
from libs.statsParser.models import Player

class TestPlayer(TestCase):
  def test_creating_a_new_player_and_saving_it_to_the_database(self):
    player = Player()

    player.name = "Ken Tam"
    player.team = "CHI"
    player.owner = "Me"
    player.position = "WR"
    player.points = 123.2
    player.averagePoints = 122.1
    player.vbdValue = 3.3

    player.save()

    allPlayersInDB = Player.objects.all()
    self.assertEquals(len(allPlayersInDB), 1)
    self.assertEquals(allPlayersInDB[0], player)

    self.assertEquals(allPlayersInDB[0].name, "Ken Tam")
    self.assertEquals(allPlayersInDB[0].team, "CHI")
    self.assertEquals(allPlayersInDB[0].owner, "Me")
    self.assertEquals(allPlayersInDB[0].position, "WR")
    self.assertEquals(allPlayersInDB[0].points, 123.2)
    self.assertEquals(allPlayersInDB[0].averagePoints, 122.1)
    self.assertEquals(allPlayersInDB[0].vbdValue, 3.3)


