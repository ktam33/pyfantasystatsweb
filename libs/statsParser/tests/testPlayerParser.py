from django.test import TestCase
from libs.statsParser import PlayerParser

class TestPlayerParser(TestCase):
  
  htmlTemplate = "<html><table id='players-table'><thead></thead>\
      <tbody>{0}</tbody>\
      </table></html>"

  playerRowTemplate = "<tr><td><a>{0}</a><span>(CHI - WR)</span></td>\
      <td></td><td></td><td></td><td>Me</td>\
      <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>\
      <td></td><td></td><td></td><td></td><td></td><td>123.10</td></tr>"

  sut = PlayerParser()

  def test_will_parse_player_stats_from_html(self):
    playerHtml = self.playerRowTemplate.format("Ken Tam")

    html = self.htmlTemplate.format(playerHtml)

    result = self.sut.parse(html)[0]

    self.assertEquals(result.name, "Ken Tam")
    self.assertEquals(result.team, "CHI")
    self.assertEquals(result.position, "WR")
    self.assertEquals(result.owner, "Me")
    self.assertEquals(result.points, 123.1)

  def test_will_parse_multiple_players_from_html(self):
    
    playerOneHtml = self.playerRowTemplate.format("Player One")
    playerTwoHtml = self.playerRowTemplate.format("Player Two")
    
    html = self.htmlTemplate.format(playerOneHtml + playerTwoHtml)

    results = self.sut.parse(html)
    
    self.assertEquals(len(results), 2)
    self.assertEquals(results[0].name, "Player One")
    self.assertEquals(results[1].name, "Player Two")

  def test_will_take_first_position_if_multiple_exist(self):
    playerHtml = "<tr><td><a>asdf</a><span>(CHI - RB,TE)</span></td>\
      <td></td><td></td><td></td><td>Me</td>\
      <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>\
      <td></td><td></td><td></td><td></td><td></td><td>123.10</td></tr>"

    html = self.htmlTemplate.format(playerHtml)

    result = self.sut.parse(html)[0]
   
    self.assertEquals(result.position, "RB")
    
  