from django.test import TestCase
from mock import MagicMock
from mock import patch
from libs.statsParser.models import Player
from libs.statsParser import LoadController

class TestLoadController(TestCase):

  

  def test_parse_html_for_players_starting_with_offset(self):
    playerMock = MagicMock()
    players = [playerMock, playerMock, playerMock]

    sut = LoadController()
    sut.webClient = MagicMock()
    sut.playerParser = MagicMock()
    sut.webClient.getPlayerStats.return_value = '<html></html>'
    sut.playerParser.parse.return_value = players

    # Act
    result = sut.parseOffset('ktam', 'pwd123', 50)

    sut.webClient.login.assert_called_with('ktam', 'pwd123')
    sut.webClient.getPlayerStats.assert_called_with(50)
    sut.playerParser.parse.assert_called_with('<html></html>')

    self.assertEquals(players, result)
    self.assertEquals(len(playerMock.save.mock_calls), 3)

  def test_will_delete_all_players_when_beginning_to_parse(self):
    with patch('libs.statsParser.models.Player.objects.all') as mockPlayers:
      
      sut = LoadController()
      sut.webClient = MagicMock()
      sut.playerParser = MagicMock()

      # Act
      sut.parseOffset("asdf", "password", 0)

      mockPlayers.return_value.delete.assert_called_with()


  def test_will_not_delete_all_players_when_in_the_middle_of_parsing(self):
    with patch('libs.statsParser.models.Player.objects.all') as mockPlayers:
      
      sut = LoadController()
      sut.webClient = MagicMock()
      sut.playerParser = MagicMock()

      # Act
      sut.parseOffset("asdf", "password", 59)

      self.assertFalse(mockPlayers.return_value.delete.called)
