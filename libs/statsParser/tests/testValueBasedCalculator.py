from django.test import TestCase
from mock import MagicMock
from libs.statsParser.models import Player
from libs.statsParser import ValueBasedCalculator

class TestValueBasedCalculator(TestCase):
  
  sut = ValueBasedCalculator(MagicMock())

  def test_will_calculate_average_correctly(self):
    players = [Player(name="player1", points=50),
               Player(name="player2", points=52),]

    self.sut.leagueSettings.getWeeksRemaining.return_value = 5
    self.sut.leagueSettings.getTeamsWithByes.return_value = []

    # Act
    self.sut.calculateAveragePoints(players)

    self.assertEquals(10, players[0].averagePoints)
    self.assertEquals(10.4, players[1].averagePoints)

  def test_will_account_for_bye_week(self):
    players = [Player(name="player1", points=50, team="CHI"),
               Player(name="player2", points=25),]

    self.sut.leagueSettings.getWeeksRemaining.return_value = 6
    self.sut.leagueSettings.getTeamsWithByes.return_value = ["CHI", "SF"]

    # Act
    self.sut.calculateAveragePoints(players)

    self.assertEquals(10, players[0].averagePoints)
    
  def test_will_calculate_value_based_points(self):

    player1 = Player(name="player1", averagePoints=12, position="RB")
    player2 = Player(name="player1", averagePoints=20.5, position="QB")

    players = [
                Player(name="filler", averagePoints=18.5, position="RB"),
                Player(name="filler", averagePoints=17.5, position="RB"),
                Player(name="filler", averagePoints=16.5, position="RB"),
                Player(name="filler", averagePoints=15.5, position="RB"),
                Player(name="filler", averagePoints=14.5, position="RB"),
                Player(name="baseline", averagePoints=13.5, position="RB"),
                Player(name="filler", averagePoints=12.5, position="RB"),
                player1,
              ]

    players.extend(
                    [
                      Player(name="filler", averagePoints=21.5, position="QB"),
                      Player(name="filler", averagePoints=20.5, position="QB"),
                      player2,
                      Player(name="filler", averagePoints=19.5, position="QB"),
                      Player(name="baseline", averagePoints=18.5, position="QB"),
                      Player(name="filler", averagePoints=15.5, position="QB"),
                      Player(name="filler", averagePoints=10.5, position="QB"),
                      Player(name="filler", averagePoints=9.5, position="QB"),
                    ]  
                  )

    def side_effect(*args):
      if args[0] == 'QB': 
        return 5
      if args[0] == 'RB': 
        return 6
      return 7

    self.sut.leagueSettings.getWorstStarterPosition.side_effect = side_effect

    # Act
    self.sut.calculateValueBasedPoints(players)

    self.assertEquals(-1.5, player1.vbdValue)
    self.assertEquals(2, player2.vbdValue)