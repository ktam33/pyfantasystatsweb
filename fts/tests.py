from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

class FanStatsTest(LiveServerTestCase):
  def setUp(self):
    self.browser = webdriver.Firefox()
    self.browser.implicitly_wait(3)

  def tearDown(self):
    self.browser.quit()

  def test_can_generate_stats(self):
    self.browser.get(self.live_server_url)

    body = self.browser.find_element_by_tag_name('body')
    self.assertIn('Provide Yahoo login credentials', body.text)

    userNameField = self.browser.find_element_by_id('username')
    userNameField.send_keys('ktam')
    passwordField = self.browser.find_element_by_id('password')
    passwordField.send_keys('mypassword')

    submitButton = self.browser.find_element_by_css_selector("input[type='submit']")
    submitButton.click()

    progressBar = self.browser.find_element_by_id('statsProgress')
    WebDriverWait(self.browser, 60).until(lambda d : d.find_element_by_id('statsLoaded'))

    body = self.browser.find_element_by_tag_name('body')
    self.assertIn('Stats have been successfully loaded', body.text)
