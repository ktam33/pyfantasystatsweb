from django.conf.urls import patterns, include, url
from apps.web.views import home
from apps.web.views import loadAll
from apps.web.views import listAll
from apps.web.views import refreshStats
from apps.web.views import loadAuctionValues
from apps.web.views import refreshAuction

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
  url(r'^$', home),
  url(r'^getAll/$', loadAll),
  url(r'^listAll/$', listAll),
  url(r'^getAuctionValues/$', loadAuctionValues),
  url(r'^refreshAuction/$', refreshAuction),
  url(r'^refreshStats/$', refreshStats),
)
