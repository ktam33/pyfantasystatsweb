from django.shortcuts import render
from django.http import HttpResponse
from django.utils import simplejson
from libs.statsParser import LoadController
from libs.statsParser.models import Player
from libs.statsParser.tasks import loadStats
from libs.statsParser.tasks import loadAuction
import jsonpickle
from models import ListAllViewModel
from models import LoadPlayersViewModel


def listAll(request):

  controller = LoadController() 

  results = controller.getAll()

  players = list(results[1])
  #players = [player for player in list(results[1]) if player.projectedAuctionValue > 0]
  owners = results[0]
  lastSaved = results[2]

  model = ListAllViewModel(owners, players, lastSaved)
  return HttpResponse(simplejson.dumps(jsonpickle.encode(model)), mimetype="application/json")

def home(request):
  return render(request, 'main.html')

def refreshStats(request):
  loadStats.delay();
  return HttpResponse('stat refresh started', mimetype="application/json")

def refreshAuction(request):
  loadAuction.delay();
  return HttpResponse('auction stat refresh started', mimetype="application/json")

def loadAll(request):
  username = request.POST['username']
  password = request.POST['password']
  offset = int(request.POST['offset'])

  if offset < 500:
    players = LoadController().parseOffset(username, password, offset)
    model = LoadPlayersViewModel(players, offset + len(players))
    return HttpResponse(simplejson.dumps(jsonpickle.encode(model)), mimetype="application/json")
  else:
    players = LoadController().parseDefense(username, password)
    model = LoadPlayersViewModel(players, offset + len(players))
    return HttpResponse(simplejson.dumps(jsonpickle.encode(model)), mimetype="application/json")

def loadAuctionValues(request):
  username = request.POST['username']
  password = request.POST['password']
  results = LoadController().loadAuctionValues(username, password)
  players = list(results[1])
  owners = results[0]
  model = ListAllViewModel(owners, players)
  return HttpResponse(simplejson.dumps(jsonpickle.encode(model)), mimetype="application/json")  
  