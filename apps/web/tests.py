from django.test import TestCase
from libs.statsParser.models import Player

class MainPageViewTest(TestCase):
  def test_root_url_prompts_for_username_and_password(self):
    response = self.client.get('/')

    self.assertTemplateUsed(response, 'main.html')
    self.assertIn('Provide Yahoo login credentials', response.content)





