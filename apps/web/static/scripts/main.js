ko.bindingHandlers.rounded = {
  update: function(element, valueAccessor, allBindingsAccessor) {
    var value = ko.utils.unwrapObservable(valueAccessor())
    var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || 2
    var formattedValue = value.toFixed(precision);
    ko.bindingHandlers.text.update(element, function(){
      return formattedValue;
    });
  }
};

function MainViewModel(endpoints, token) {
  self = this;

  self.progress = ko.observable(0);
  self.someValue = ko.observable();
  self.username = ko.observable();
  self.password = ko.observable();
  self.lastSaved = ko.observable();
  self.offset = ko.observable(0);
  self.errorMessage = ko.observable();
  self.players = ko.observableArray();
  self.owners = ko.observableArray();
  self.positions = ['QB', 'RB', 'WR', 'TE', 'K', 'DEF'];
  self.selectedPositions = [];
  self.showDrafted = ko.observable(true);
  self.oTable = null;

  
  self.progressBar = ko.computed(function() {
    if (self.progress() > 0)
    {
      $("#progressbar").progressbar({
        value: self.progress()
      });
    }      
  });
  
  self.listAll = function() {
    $.ajax({
      type: 'POST',
      url: endpoints.listAll,
      data: { csrfmiddlewaretoken: token },
      success: function(data) {
          self.someValue("");
          self.errorMessage("");
          
          if ($("#progressbar").data("uiProgressbar")) {
              $( "#progressbar" ).progressbar("destroy");
          };
          

          var response = jQuery.parseJSON(data)

          for (index = 0; index < response.players.length; ++index) {
            response.players[index].drafted = ko.observable(false);
          }
          self.players(response.players);
          self.owners(response.owners);
          self.lastSaved(response.lastSaved);
  
          $("#owners").multiselect({
            height: "auto",
            click: self.processFilters,
            checkAll: self.processFilters,
            uncheckAll: self.processFilters
          });

          $("#positions").multiselect({
            height: "auto",
            click: self.processFilters,
            checkAll: self.processFilters,
            uncheckAll: self.processFilters
          });


          self.oTable = $('#players').dataTable({
            "bPaginate" : false,
            "aaSorting": [[ 7, "desc" ]]
          });

        },
      error: function(data) {
          if (data.responseText)
          {
            self.errorMessage(data.responseText);
          }
          else
          {
            self.errorMessage('Login failed. Please try again.');  
          }
          
        },
      datatype: "json",
    });

  };

  self.getStats = function() {
    $.ajax({
      type: 'POST',
      url: endpoints.loadStats,
      data: { username: self.username(), password: self.password(), offset: self.offset(), csrfmiddlewaretoken: token },
      success: function(data) {
          var response = jQuery.parseJSON(data)
          self.progress(response.percentage);
          self.someValue(data);
          if (self.progress() <= 100)
          {
            self.offset(response.nextPlayerToLoad);
            self.getStats();
          }
        },
      error: function(data) {
          if (data.responseText)
          {
            self.errorMessage(data.responseText);
          }
        },
      datatype: "json",
    });
  };

  self.loadAuctionValues = function() {
    $.ajax({
      type: 'POST',
      url: endpoints.loadAuctionValues,
      data: { username: self.username(), password: self.password(), csrfmiddlewaretoken: token },
      success: function(data) {
          var response = jQuery.parseJSON(data)
          self.players(response.players);
          $('#players').dataTable();
        },
      error: function(data) {
          if (data.responseText)
          {
            self.errorMessage(data.responseText);
          }
        },
      datatype: "json",
    });
  };

  self.processFilters = function(){
    var selectedOwners = $("#owners").multiselect("getChecked").map(function(){
       return this.value;    
    }).get();

    var selectedPositions = $("#positions").multiselect("getChecked").map(function(){
       return this.value;    
    }).get();

    self.oTable.fnFilter( 
      selectedOwners.join('|'),
       4, 
       true, 
       false
    );

    self.oTable.fnFilter( 
      selectedPositions.join('|'),
       3, 
       true, 
       false
    );
  };

}



