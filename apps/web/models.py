class LoadPlayersViewModel:
  def __init__(self, players, nextPlayerToLoad):
    self.players = players
    self.nextPlayerToLoad = nextPlayerToLoad
    self.percentage = (nextPlayerToLoad / 500.0) * 100

class ListAllViewModel:
  def __init__(self, owners, players, lastSaved):
    self.players = players
    self.owners = owners
    self.lastSaved = lastSaved
